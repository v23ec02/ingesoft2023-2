from django.shortcuts import render, HttpResponse
from .models import *

def index(request):

    listaGrupo1 = Estudiante.objects.filter(grupo=1)
    listaGrupo4 = Estudiante.objects.filter(grupo=4)
    listaGomez = Estudiante.objects.filter(apellidos = 'Gómez')
    anos22 = Estudiante.objects.filter(edad = 22)
    anos22EnGrupo3 = Estudiante.objects.filter(grupo = 3, edad = 22)
    listaTodos = Estudiante.objects.all();

    return render(request, 'index.html', {'listaGrupo1':listaGrupo1, 'listaGrupo4':listaGrupo4, 'apellidosGomez':listaGomez, 'listaMismaEdad':anos22, 'anos22EnGrupo3':anos22EnGrupo3, "listaTodos":listaTodos} )
